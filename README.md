## gstLAL Benchmarking Tools

This is a growing collection of scripts and Makefiles to generate profling and benchmarking tools for gstLAL. They are based around a singularity container approach for portability across systems, without having to build code.

### Simple Tasks:

What follows is a brief description of commands to run to generate results.

#### Profiling with ``perf``:

``perf`` records kernel calls and then produces a collection of output that describes what symbols (like what commands the computer is running) and via what images (the libraries that are being loaded and called). This step is typically used to ensure that a code is built and linked correctly against the desired libraries. Also to ensure that the functions that are being called are of the desired performance. 

Usage:
`make -f Makefile.profiling perf`

Produces two files, the indicative content of which is below: 

``gstlal_images.dat``: Shows the libraries that are being called, and the percentage of the runtime that are occupying the gstLAL run:

```
# Overhead  Shared Object
# ........  ..............................................................................................
#
    26.10%  /usr/lib64/libpython3.6m.so.1.0
    24.74%  /opt/intel/compilers_and_libraries_2020.0.166/linux/mkl/lib/intel64_lin/libmkl_def.so
    12.12%  /usr/lib64/libc-2.17.so
     9.77%  /usr/lib64/gstreamer-1.0/libgstgstlal.so
     5.28%  /usr/lib64/libgstlal.so.1.0.0
     4.41%  /usr/lib64/libfftw3f.so.3.6.10
     3.69%  /usr/lib64/libgsl.so.0.16.0
     2.18%  /usr/lib64/libm-2.17.so
```
In the above example, the code spent a majority of time in the libpython and Intel MKL libraries. 

``gstlal_symbols.dat``: This file is typically longer (thousands of lines) and shows what symbols are being run, from what library, and what percentage of the runtime it was taking up. For example: 

```
     0.66%  gstlalitacac4:s  /usr/lib64/libc-2.17.so     
     0x14cf91           B [.] __memcpy_ssse3
```
In this case, the code spent 0.66% of its runtime performing a ssse3 memcpy (which is in libc), called from gstlalitacac. Note there are going to be hundreds of such lines, so finding cumulative data is best left to the use of ``sed`` and ``awk``.

#### Sweeping over SVD banks

gstLAL has shown sensitivity to the number of templates in an SVD decomposition and to the mass range of the templates. There are scripts in the `scripts` directory to sweep over the included set of banks (in ``BANK_DIR``) and generate tabulated throughput numbers for plotting. An example plotting script is in the ``plotting`` directory, with more to come. 

#### Saturation Throughput

This will determine the number of cores on the current machine, and launch exactly that many gstLAL jobs, record the runtime and then calculate the throughput on the machine. This funciton is largely for legacy purposes.

Usage:
`make -f Makefile.profiling throughput`

After a time to run, the code will produce a line like; 

`Result is XXX templates/core.`
