#!/bin/bash 

# This script is intended to launch many gstlal_inspiral jobs simultaneously,
# compute the throughput per core of each job, dump it to a file, and then make
# a histogram. 

# Intended usage:
# $scripts/saturation.sh
# run in the root gstlal-benchmarking directory.

# I guess a 'gotcha' right now assumes that you already ran the profiling Makefile
# and generated fake data, a segments file, a frames cache, and a run script. Fix
# that after finishing this. 

# Inputs:
# Path to singularity container folder (absolute or relative to current folder):
sing_container=gstlal_container
# Set the maximum number of jobs to run, for memory reasons. set < 1 for no maximum
max_jobs=-1

# Starting the run logic. Start by getting data from lscpu. Just run the command
# multiple times, whatever. 

threads=$(lscpu | sed -n 's/Thread(s) per core:[ \t]*//p')
cpus=$(expr $(lscpu | sed -n 's/CPU(s):[ \t]*//p' | head -1) - 1)

# Do some setup:
rm -f run_jobs
touch run_jobs
chmod +x run_jobs

# Clear and rebuild the output directory:
rm -rf saturation_output
mkdir -p saturation_output

gstlal_command="singularity exec $sing_container gstlal_inspiral \
        --ht-gate-threshold 15.235  \
        --track-psd  \
        --control-peak-time 0 \
        --psd-fft-length 4  \
        --channel-name H1=FAKE  \
        --channel-name L1=FAKE  \
        --channel-name V1=FAKE  \
        --tmp-space /tmp  \
        --coincidence-threshold 0.005  \
        --fir-stride 0.25  \
        --min-instruments 1  \
        --far-trials-factor 1  \
        --compress-ranking-stat  \
        --compress-ranking-stat-threshold 0.003  \
        --disable-service-discovery  \
        --data-source frames  \
        --frame-cache frames.cache \
        --frame-segments-name datasegments \
        --frame-segments-file output/segments.xml.gz \
        --gps-start-time 1000000000 \
        --gps-end-time 1000025000 \
        --reference-psd online_static/H1L1V1-GSTLAL_REFERENCE_PSD-0-0.xml.gz  \
        --time-slide-file online_static/tisi.xml  \
        --output /dev/null  \
        --ranking-stat-output /dev/null  \
        --zerolag-rankingstat-pdf online_static/H1L1V1-0011_GSTLAL_ZEROLAG_DIST_STAT_PDFS-0-0.xml.gz \
	--svd-bank run23_o4banks/H1-0750_GSTLAL_SVD_BANK-0-0.xml.gz \
	--svd-bank run23_o4banks/L1-0750_GSTLAL_SVD_BANK-0-0.xml.gz \
	--svd-bank run23_o4banks/V1-0750_GSTLAL_SVD_BANK-0-0.xml.gz "
        

# Start looping to generate the run file:
for i in $(seq 0 $threads $cpus); do
  # If hyperthreading is on, then run on two adjacent cores. If not,
  # then just lock to a single core. 
  ts="taskset -c $i"
  if [ $threads -gt 1 ]; then
    ts=$ts",$(expr $i + 1)"
  fi

  # Define the time output file:
  output_file=saturation_output/runtime.$i
 
 echo $ts /usr/bin/time -f '"%e\n%M"' -o $output_file $gstlal_command >> run_jobs
done

# trim the number of jobs, in necessary. 
if [ $max_jobs -gt 0 ]; then
  head -$max_jobs run_jobs > tmp && mv tmp run_jobs
fi

# define the number of cores to run on and run it with gnu parallel:
parallel_jobs=$(cat run_jobs | wc -l)
parallel --jobs $parallel_jobs < run_jobs

# Get number of templates:
templates=$(singularity exec $sing_container ligolw_print -t sngl_inspiral -c mass1 run23_o4banks/H1-0750_GSTLAL_SVD_BANK-0-0.xml.gz | wc -l)

# when its finished, make an output file:
output_file=saturation_output/gstlal_saturation_output.dat
echo "run on $(hostname) completed on $(date -R)" > $output_file

for f in saturation_output/runtime.*; do
    # do some arithmetic
    runtime=$(head -n1 $f)
    memory=$(tail -n1 $f)
    echo "Profiling job finished in $runtime seconds"
    echo "svd bank has $templates templates"
    echo "simulation time was 25000 seconds"

    throughput_sc=$(echo "3 * $templates * 25000 / $runtime" | bc)
    echo "single core throughput is $throughput_sc templates/core"

    echo "$templates      $runtime      $throughput_sc     $memory" >>  $output_file
done

