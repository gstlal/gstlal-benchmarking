#!/bin/bash

# Obligatory...
GSTLAL_FIR_WHITEN=0

# Pick two cores to run on. This is for hyperthreaded systems, where 
# two hyperthreaded cores comprise one physical core. Ensure that the 
# cores are on the same NUMA node. 

core_1=0
core_2=1

# Specify a svd dir, without a trailing slash:
svd_dir=/home/albert.einstein/bank_dir/filter/svd_bank
base_output_dir=bank_dir

# Specify the increment to skip, since there can be thousands of files in a 
# svd directory.
incr=15

# Get the number of svds in the directory:
num_svds=$(ls $svd_dir/H1-* | wc -l)

#Set up:
mkdir -p $base_output_dir
echo $svd_dir > $base_output_dir/single_core_throughput.out

for svd in $(seq -f "%04g" 0 50 $num_svds)
do
    # Construct the svd-bank strings:
    svd_bank_args=""
    for ifo in H1 L1 V1
    do
        thisline="--svd-bank "$svd_dir"/"$ifo"-"$svd"_GSTLAL_SVD_BANK-0-0.xml.gz  "
        svd_bank_args=$svd_bank_args$thisline
    done

    # Get number of templates:
    templates=$(singularity exec gstlal_container ligolw_print -t sngl_inspiral -c mass1 "$svd_dir"/H1-"$svd"_GSTLAL_SVD_BANK-0-0.xml.gz | wc -l)

    # Get the max chirp mass:
    max_chirp=$(singularity exec gstlal_container ligolw_print -t sngl_inspiral -c mchirp "$svd_dir"/H1-"$svd"_GSTLAL_SVD_BANK-0-0.xml.gz | sort -n | tail -1)

    # Get the min chirp mass:
    min_chirp=$(singularity exec gstlal_container ligolw_print -t sngl_inspiral -c mchirp "$svd_dir"/H1-"$svd"_GSTLAL_SVD_BANK-0-0.xml.gz | sort -n | head -1)

    # Make an output directory to store perf data, reports. and run times.
    echo "running $svd"
    mkdir -p $base_output_dir/$svd

    echo "singularity exec gstlal_container gstlal_inspiral \
	--ht-gate-threshold 15.235  \
	--track-psd  \
	--control-peak-time 0 \
	--psd-fft-length 4  \
	--channel-name H1=FAKE  \
	--channel-name L1=FAKE  \
	--channel-name V1=FAKE  \
	--tmp-space /tmp  \
	--coincidence-threshold 0.005  \
	--fir-stride 0.25  \
	--min-instruments 1  \
	--far-trials-factor 1  \
	--compress-ranking-stat  \
	--compress-ranking-stat-threshold 0.003  \
	--disable-service-discovery  \
	--data-source frames  \
	--frame-cache frames.cache \
	--frame-segments-name datasegments \
	--frame-segments-file output/segments.xml.gz \
        --gps-start-time 1000000000 \
	--gps-end-time 1000025000 \
	--reference-psd online_static/H1L1V1-GSTLAL_REFERENCE_PSD-0-0.xml.gz  \
	--time-slide-file online_static/tisi.xml  \
	--output /dev/null  \
	--ranking-stat-output /dev/null  \
	--zerolag-rankingstat-pdf online_static/H1L1V1-0011_GSTLAL_ZEROLAG_DIST_STAT_PDFS-0-0.xml.gz "$svd_bank_args  > run_script_$base_output_dir

    chmod +x run_script_$base_output_dir
    /usr/bin/time -f "%e\n%M" -o $base_output_dir/$svd/runtime taskset -c $core_1,$core_2 ./run_script_$base_output_dir | tee $base_output_dir/$svd/gstlal.output

    # move files:
    mv run_script_$base_output_dir $base_output_dir/$svd/

    # do some arithmetic
    runtime=$(head -n1 $base_output_dir/$svd/runtime)
    memory=$(tail -n1 $base_output_dir/$svd/runtime)
    echo "Profiling job finished in $runtime seconds"
    echo "svd bank has $templates templates"
    echo "simulation time was 25000 seconds"

    throughput_sc=$(echo "3 * $templates * 25000 / $runtime" | bc)
    echo "single core throughput is $throughput_sc templates/core"

    echo "$svd     $max_chirp      $min_chirp     $templates      $runtime      $throughput_sc     $memory" >> $base_output_dir/single_core_throughput.out
    
done
