#!/bin/bash

GSTLAL_FIR_WHITEN=0

# Pick two cores to run on:
core_1=174
core_2=175

mass_ranges=("BNS" "IMBH" "BBH")
num_templates=(400 600 800 1000 1200 1400 1600 1800 2000)

job_name="baseline"

mkdir -p $job_name
touch $job_name/single_core_throughput.out

for mass in "${mass_ranges[@]}"; do
for templates in "${num_templates[@]}"; do

    outdir=$job_name/"$mass"_$templates

    # Make an output directory to store perf data, reports. and run times.
    echo "running $outdir"
    mkdir -p $outdir

    # Get number of templates:
    templates=$(singularity exec gstlal_container ligolw_print -t sngl_inspiral -c mass1 BANK_DIR/H1-GSTLAL_SVD_BANK_"$mass"_$templates-0-0.xml.gz | wc -l)

    # Get the max chirp mass:
    max_chirp=$(singularity exec gstlal_container ligolw_print -t sngl_inspiral -c mchirp BANK_DIR/H1-GSTLAL_SVD_BANK_"$mass"_$templates-0-0.xml.gz | sort -n | tail -1)

    # Get the min chirp mass:
    min_chirp=$(singularity exec gstlal_container ligolw_print -t sngl_inspiral -c mchirp BANK_DIR/H1-GSTLAL_SVD_BANK_"$mass"_$templates-0-0.xml.gz | sort -n | head -1)

    # run it:
    echo " singularity exec gstlal_container gstlal_inspiral \
        --ht-gate-threshold 15.235  \
        --track-psd  \
        --control-peak-time 0 \
        --psd-fft-length 4  \
        --channel-name H1=FAKE  \
        --channel-name L1=FAKE  \
        --channel-name V1=FAKE  \
        --tmp-space /tmp  \
        --coincidence-threshold 0.005  \
        --fir-stride 0.25  \
        --min-instruments 1  \
        --far-trials-factor 1  \
        --compress-ranking-stat  \
        --compress-ranking-stat-threshold 0.003  \
        --disable-service-discovery  \
        --data-source frames  \
        --frame-cache frames.cache \
        --frame-segments-name datasegments \
        --frame-segments-file output/segments.xml.gz \
        --gps-start-time 1000000000 \
        --gps-end-time 1000025000 \
        --reference-psd online_static/H1L1V1-GSTLAL_REFERENCE_PSD-0-0.xml.gz  \
        --time-slide-file online_static/tisi.xml  \
        --output /dev/null  \
        --ranking-stat-output /dev/null  \
        --zerolag-rankingstat-pdf online_static/H1L1V1-0011_GSTLAL_ZEROLAG_DIST_STAT_PDFS-0-0.xml.gz \
        --svd-bank BANK_DIR/H1-GSTLAL_SVD_BANK_"$mass"_$templates-0-0.xml.gz \
        --svd-bank BANK_DIR/L1-GSTLAL_SVD_BANK_"$mass"_$templates-0-0.xml.gz \
        --svd-bank BANK_DIR/V1-GSTLAL_SVD_BANK_"$mass"_$templates-0-0.xml.gz " > run_script_$job_name

    chmod +x run_script_$job_name
    /usr/bin/time -f "%e\n%M" -o $outdir/runtime taskset -c $core_1,$core_2 ./run_script_$job_name | tee $outdir/gstlal.output

    # move files:
    mv run_script_$job_name $outdir/

    # do some arithmetic
    runtime=$(head -n1 $outdir/runtime)
    memory=$(tail -n1 $outdir/runtime)
    echo "Profiling job finished in $runtime seconds"
    echo "svd bank has $templates templates"
    echo "simulation time was 25000 seconds"

    throughput_sc=$(echo "3 * $templates * 25000 / $runtime" | bc)
    echo "single core throughput is $throughput_sc templates/core"

    echo "$svd     $max_chirp      $min_chirp     $templates      $runtime      $throughput_sc     $memory" >> $job_name/single_core_throughput.out
   
done
done
